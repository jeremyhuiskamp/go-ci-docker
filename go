#!/bin/sh

# Executes sub-commands from the .goscripts directory,
# bootstrapping into the tools docker image if necessary.

set -eu

if [ ! -f /etc/this-is-the-build-image ]; then
  # running directly on a developer machine:
  # re-execute ourselves inside the container
  cd tools-image
  docker-compose build --quiet 2> /dev/null
  docker-compose run --rm tools ./go "$@"
  exit $?
fi

# From now on, we're definitely in the build image

if [ "$#" != "0" ]; then
  COMMAND="$1"
  shift
else
  COMMAND=help
fi

COMMAND_SCRIPT="./.goscripts/${COMMAND}.sh"
if [ ! -f "$COMMAND_SCRIPT" ]; then
  echo "Unknown command: '$COMMAND'"
  COMMAND_SCRIPT="./.goscripts/help.sh"
fi

"$COMMAND_SCRIPT" "$@"
