# Go Script and CI in Docker

The scenario: you're using the
[go script](https://www.thoughtworks.com/insights/blog/praise-go-script-part-i) pattern so that all of your
automation runs consistently, both locally and in your pipeline.  This
is great because you can test out your pipeline changes before
committing and running them in the shared pipeline.

However, there is a bit of a mismatch between the local and the shared pipeline:
- in the shared pipeline (gitlab in this case), you are running your tasks
  in a docker container, which you have source controlled and which has
  consistent versions of your tools
- on your local machine, you want to run the tasks from your own terminal,
  which means you need matching versions of all the tools installed and
  kept up to date on every developer's machine

So the pattern implemented in this repository is the following:
- if the `go` script is invoked within the docker container, it just runs
- if the `go` script is invoked outside the docker container, it starts
  the container and re-runs an identical copy of itself inside.

Now when you run, for example, `./go build` in your terminal, it runs
exactly the same way as if invoked in a gitlab pipeline, even if you
have none of the tooling (except docker and a shell) installed locally.

## Things to keep in sync

There are a few things that are different between local and gitlab, which
need to be implemented twice to keep them the same:

- building the image
  - the `go` script triggers rebuilding the image before starting it
    - this is relatively fast due to the local daemon detecting when nothing
      changed
  - the gitlab pipeline builds the image in a preliminary stage
- mounting the source code
  - gitlab mounts the source at `/build/<project>`
  - `go` mounts it at `/build`
  - the difference is probably not important, and it's not worth figuring out
    the project name in the local run to ensure that the path is identical
- setting the working directory to the root of the repo

## Open Questions

- if you need different build tasks to run in different images...
  - with gitlab you need to mention the image explicitly anyway
  - the `go` script would need a mapping of task to image that it can read
    before bootstrapping the container
- how can this pattern leverage docker-compose for integration testing with
  containerised dependencies?

## Alternatives

- the `go` script could unconditionally start a new docker container
  - on gitlab, this would require docker-in-docker which is pretty slow
- force developers to do everything locally in a longer-running docker container
  - if they're ok with leaving their own customised shell behind, this
    could work
  - it might not work well if different tasks require different images
