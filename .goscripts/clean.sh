#!/bin/sh

set -eu

SHORT_HELP="remove all build output"
. ./.goscripts/.help.sh

rm -rf ./build
