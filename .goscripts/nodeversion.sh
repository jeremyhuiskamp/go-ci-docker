#!/bin/sh

set -eu

SHORT_HELP="show the versions of node and npm in the build image"
. ./.goscripts/.help.sh

node --version
npm --version
