#!/bin/sh

set -eu

export SHORT_HELP="execute tests"
export FULL_HELP="
no arguments currently supported"
. ./.goscripts/.help.sh

mkdir -p build/report

# just make sure the artifact is as we expect:
grep -q software build/artifact 2>&1 | tee build/report/test.txt
