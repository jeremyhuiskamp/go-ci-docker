#!/bin/sh

set -eu

SHORT_HELP="run npm with the given arguments in the build container"
. ./.goscripts/.help.sh

npm "$@"
