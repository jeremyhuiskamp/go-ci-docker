# Include this file from a script to handle help options.
# Define $SHORT_HELP to go in the help summary.
# Define $FULL_HELP to print all the arguments for a script.

if [ "$#" != "0" ]; then
  case "$1" in
    --short-help)
      echo "${SHORT_HELP:-}"
      exit 0
      ;;
    --full-help)
      echo "${FULL_HELP:-$(printf '\nTODO: write help text')}"
      exit 0
  esac
fi
