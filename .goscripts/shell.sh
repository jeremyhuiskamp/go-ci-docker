#!/bin/sh

set -eu

SHORT_HELP="start a shell in the build container"
. ./.goscripts/.help.sh

/bin/sh
