# `.goscripts`

Files in this directory serve as sub-commands for the top-level `./go` script.

All `<foo>.sh` files can be executed as `./go <foo> ...args...`.  They should have their executable bit set.  They run in their own process, but their exit code is propagated.  They always run in the root directory of the repo, inside the tools-image container.

All other files, such as this README, will be ignored.
