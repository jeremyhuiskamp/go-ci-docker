#!/bin/sh

set -eu

SHORT_HELP="build the software"
. ./.goscripts/.help.sh

rm -rf ./build
mkdir build
echo software > build/artifact
