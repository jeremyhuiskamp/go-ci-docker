#!/bin/sh

set -eu

SHORT_HELP="get help about other commands"
FULL_HELP="
go help
    - lists all commands available
go help <command>
    - prints usage information for <command>"
. ./.goscripts/.help.sh

if [ "$#" != 0 ]; then
  SCRIPT="./.goscripts/${1}.sh"
  if [ -x "$SCRIPT" ]; then
    echo "${1}: $("${SCRIPT}" --short-help)"
    "$SCRIPT" --full-help
    exit 0
  fi
  echo "help: unknown command: ${1}"
  exit 1
fi

echo These commands are available:
echo

for f in ./.goscripts/*.sh; do
  printf '  %-12s %s\n' "$(basename "$f" .sh)" "$($f --short-help)"
done
