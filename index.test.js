const magicNumberTrick = require('.');

describe('magic number trick', () => {
    it('always returns 3', () => {
        const input = Math.floor(Math.random() * 100);
        const output = magicNumberTrick(input);
        expect(output).toEqual(3);
    })
});