const magicNumberTrick = anyNumber =>
    ((anyNumber + 1) * 2 + 4) / 2 - anyNumber;

module.exports = magicNumberTrick;